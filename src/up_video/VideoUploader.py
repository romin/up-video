import subprocess
import re
import os
import random

currentDir = os.path.dirname(os.path.realpath(__file__))


class VideoUploader:

    def __init__(self) -> None:
        self.acountList = ["cloud_eve", "haitao"]

    def upload(self, video):
        path = video.path
        title = video.title
        acount = self.selectAcount()
        try:
            content = subprocess.check_output(["youtube-upload", "--title", title,
                                               "--client-secrets",
                                               "%s/client_secret_%s.json" % (
                                                   currentDir, acount),
                                               "--credentials-file", "%s/credentials_%s.json" % (
                                                   currentDir, acount),
                                               path])

            decode_content = content.decode("utf-8")
            search_obj = re.search(r'Video URL: (\S*)', decode_content)
            print(decode_content)
            if search_obj:
                targetUrl = search_obj.group(0)
                return UploaderInfo(path, targetUrl, acount)

        except subprocess.CalledProcessError as e:
            print(e)

    def selectAcount(self):
        return self.acountList[random.randint(0, 1)]


class UploaderInfo:

    def __init__(self, sourceUrl, targetUrl, acount) -> None:
        self.souceUrl = sourceUrl
        self.targetUrl = targetUrl
        self.acount = acount
        super().__init__()
