from datetime import datetime

from sqlalchemy import Column, String, create_engine, BIGINT, TIMESTAMP, ForeignKey, INTEGER, Sequence
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

# 创建对象的基类:
Base = declarative_base()
import os


# 定义User对象:
class Video(Base):
    # 表的名字:
    __tablename__ = 'video'
    __table_args__ = {'sqlite_autoincrement': True}

    # 表的结构:
    id = Column(INTEGER, primary_key=True, unique=True)
    site = Column(String(20))
    link = Column(String(200))
    title = Column(String(200))
    type = Column(String(20))
    size = Column(String(20))
    path = Column(String(100))
    download = Column(String(10))
    createtime = Column(TIMESTAMP, nullable=False, default=datetime.now())

    posts = relationship('Post')


class Post(Base):
    # 表的名字:
    __tablename__ = 'post'
    __table_args__ = {'sqlite_autoincrement': True}

    # 表的结构:
    id = Column(INTEGER, primary_key=True)
    vid = Column(BIGINT, ForeignKey('video.id'))
    site = Column(String(20))
    link = Column(String(200))
    acount = Column(String(100))
    createtime = Column(TIMESTAMP, nullable=False, default=datetime.now())

    video = relationship('Video')


# 初始化数据库连接:

dbPath = "%s/.up/" % os.environ['HOME']
dbFile = "video.db"
if not (os.path.exists(dbPath) and os.path.isdir(dbPath)):
    os.makedirs(dbPath)
engine = create_engine("sqlite:///" + os.path.join(dbPath, dbFile), echo=True)
# 创建DBSession类型:
DBSession = sessionmaker(bind=engine)
Base.metadata.create_all(engine)
