from urllib import request
import json

from up_video.VideoSource import VideoSource


class DirectVideoSource(VideoSource):

    def __init__(self, *args) -> None:
        self.ups = args
        super().__init__()

    def get(self):
        # print(data)
        for up in self.ups:
            req = request.Request(
                'https://space.bilibili.com/ajax/member/getSubmitVideos?mid=%s&pagesize=30&tid=0&page=1&keyword=&order=pubdate' % up)
            req.add_header('User-Agent',
                           'Mozilla/6.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/8.0 Mobile/10A5376e Safari/8536.25')
            req.add_header('Content-Type',
                           'application/json')
            with request.urlopen(req) as f:
                print('Status:', f.status, f.reason)
                for k, v in f.getheaders():
                    print('%s: %s' % (k, v))
                rsp = f.read().decode('utf-8')
                # print(json.loads(rsp)[0])
                result = ["https://www.bilibili.com/video/av%s" % rs["aid"] for rs in json.loads(rsp)["data"]["vlist"]]
                return result


if __name__ == '__main__':
    print(DirectVideoSource("26362491").get())
